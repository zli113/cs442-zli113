//
//  conversions.m
//  MP2
//
//  Created by Zeyu Li on 3/28/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import "conversions.h"
#import "DetailViewController.h"

@implementation conversions

- (void) setDefaultValue: (NSString *)chooseType
{
    double value = 100.000;
    self.resultdict = [[NSMutableDictionary alloc] init];
    NSString *pathToPlist =[[NSBundle mainBundle] pathForResource:@"conversions" ofType:@"plist"];
    self.dict =[NSDictionary dictionaryWithContentsOfFile:pathToPlist];
    self.category=[self.dict objectForKey:chooseType];
    self.celsius=[self.category objectForKey:@"Celsius"];
    self.fahrenheit=[self.category objectForKey:@"Fahrenheit"];
    self.basictype=[[self.dict objectForKey:@"Basic_Types"] objectForKey:chooseType];
    if([chooseType isEqual:@"Time"])
    {
        [self.resultdict setValue:[NSNumber numberWithDouble:value] forKey:self.basictype];
        [self.resultdict setValue:[NSNumber numberWithDouble:value*[[self.category objectForKey:@"minute"]doubleValue]] forKey:@"minute"];
        [self.resultdict setValue:[NSNumber numberWithDouble:value*[[self.category objectForKey:@"milisecond"]doubleValue]] forKey:@"milisecond"];
        [self.resultdict setValue:[NSNumber numberWithDouble:value*[[self.category objectForKey:@"second"]doubleValue]] forKey:@"second"];
    }
    if([chooseType isEqual:@"Temperature"])
    {
        [self.resultdict setValue:[NSNumber numberWithDouble:value] forKey:self.basictype];
        [self.resultdict setValue:[NSNumber numberWithDouble:value-[[self.celsius objectAtIndex:1]doubleValue]] forKey:@"Celsius"];
        [self.resultdict setValue:[NSNumber numberWithDouble:(value-[[self.fahrenheit objectAtIndex:1]doubleValue])/[[self.fahrenheit objectAtIndex:0]doubleValue]] forKey:@"Fahrenheit"];
    }
}

-(void) changeType: (NSString *)chooseType chooseUnit:(NSString *)chooseUnit value:(NSString *)value
{
    NSString *pathToPlist =[[NSBundle mainBundle] pathForResource:@"conversions" ofType:@"plist"];
    self.dict =[NSDictionary dictionaryWithContentsOfFile:pathToPlist];
    self.category=[self.dict objectForKey:chooseType];
    self.basictype=[[self.dict objectForKey:@"Basic_Types"] objectForKey:chooseType];
    double basicValue=[value doubleValue];
    if([chooseType isEqual:@"Time"])
    {
        if([chooseUnit isEqual:@"hour"])
        {
            basicValue=basicValue;
        }
        if([chooseUnit isEqual:@"minute"])
        {
            basicValue=basicValue/[[self.category objectForKey:chooseUnit]doubleValue];
        }
        if([chooseUnit isEqual:@"milisecond"])
        {
            basicValue=basicValue/[[self.category objectForKey:chooseUnit]doubleValue];
        }
        if([chooseUnit isEqual:@"second"])
        {
            basicValue=basicValue/[[self.category objectForKey:chooseUnit]doubleValue];
        }
        [self.resultdict setValue:[NSNumber numberWithDouble:basicValue] forKey:@"hour"];
        [self.resultdict setValue:[NSNumber numberWithDouble:basicValue*[[self.category objectForKey:@"minute"]doubleValue]] forKey:@"minute"];
        [self.resultdict setValue:[NSNumber numberWithDouble:basicValue*[[self.category objectForKey:@"milisecond"]doubleValue]] forKey:@"milisecond"];
        [self.resultdict setValue:[NSNumber numberWithDouble:basicValue*[[self.category objectForKey:@"second"]doubleValue]] forKey:@"second"];
    }
    if([chooseType isEqual:@"Temperature"])
    {
        if([chooseUnit isEqual:@"Kelvin"])
        {
            basicValue=basicValue;
        }
        if([chooseUnit isEqual:@"Celsius"])
        {
            basicValue=basicValue+[[self.celsius objectAtIndex:1]doubleValue];
        }
        if([chooseUnit isEqual:@"Fahrenheit"])
        {
            basicValue=basicValue*[[self.fahrenheit objectAtIndex:0]doubleValue]+[[self.fahrenheit objectAtIndex:1]doubleValue];
        }
        [self.resultdict setValue:[NSNumber numberWithDouble:basicValue] forKey:self.basictype];
        [self.resultdict setValue:[NSNumber numberWithDouble:basicValue-[[self.celsius objectAtIndex:1]doubleValue]] forKey:@"Celsius"];
        [self.resultdict setValue:[NSNumber numberWithDouble:(basicValue-[[self.fahrenheit objectAtIndex:1]doubleValue])/[[self.fahrenheit objectAtIndex:0]doubleValue]] forKey:@"Fahrenheit"];
    }
}

@end
