//
//  DetailViewController.m
//  MP2
//
//  Created by Zeyu Li on 3/28/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import "DetailViewController.h"
#import "conversions.h"

@interface DetailViewController ()
{
    NSDictionary *_unit;
    NSArray *keys;
    conversions *co;
    UIBarButtonItem *doneButton2;
    id superview;
    int selectedRow;
    NSString * selectedUnit;
    NSIndexPath *selectedIndexPath;
}

@end

@implementation DetailViewController

#pragma mark - Managing the detail item



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _unit = [self.dict objectForKey:self.title];
    keys = [[_unit allKeys] sortedArrayUsingSelector:@selector(compare:)];
    co = [[conversions alloc] init];
    [co setDefaultValue:self.title];
    doneButton2=self.navigationItem.rightBarButtonItem;
    self.navigationItem.rightBarButtonItem=nil;
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.tableView addGestureRecognizer:gestureRecognizer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _unit.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell" forIndexPath:indexPath];
    UITextField *textField=(UITextField *)[cell.contentView viewWithTag:1];
    UILabel *lable=(UILabel *)[cell.contentView viewWithTag:2];
    lable.text = keys[indexPath.row];
    //NSLog(@"%@",[co.resultdict objectForKey:@"hour"]);
    textField.text=[NSString stringWithFormat:@"%.3f", [[co.resultdict objectForKey:lable.text]doubleValue]];
    return cell;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    self.navigationItem.rightBarButtonItem=doneButton2;
    superview = textField.superview;
    while(![superview isKindOfClass:[UITableViewCell class]])
    {
        superview = [superview superview];
    }
    UITableViewCell *selectedCell = (UITableViewCell *) superview;
    selectedIndexPath = [self.tableView indexPathForCell:selectedCell];
    selectedRow = [self.tableView indexPathForCell:(UITableViewCell *)superview].row;
    selectedUnit=[keys objectAtIndex:selectedRow];
}

- (IBAction)doneButtonPressed:(id)sender {
    UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:selectedIndexPath];
    UITextField *textfield = (UITextField*)[selectedCell.contentView viewWithTag:1];
    [co changeType:self.title chooseUnit:selectedUnit value:textfield.text];
    [self.tableView reloadData];
    self.navigationItem.rightBarButtonItem=nil;
}

- (IBAction)dynamicConversion:(id)sender {
    UITableViewCell *selectedCell = [self.tableView cellForRowAtIndexPath:selectedIndexPath];
    UITextField *textfield = (UITextField*)[selectedCell.contentView viewWithTag:1];
    UILabel *lable=(UILabel *)[selectedCell.contentView viewWithTag:2];
    [co changeType:self.title chooseUnit:selectedUnit value:textfield.text];
    NSArray *cells = self.tableView.visibleCells;
    //NSLog(@"%@",co.resultdict);
    for (UITableViewCell *cell in cells) {
        UITextField *newTextfield = (UITextField*)[cell.contentView viewWithTag:1];
        UILabel *newLable=(UILabel *)[cell.contentView viewWithTag:2];
        if(![newLable.text isEqual:lable.text])
        {
            newTextfield.text=[NSString stringWithFormat:@"%.3f", [[co.resultdict objectForKey:newLable.text]doubleValue]];
        }
        //NSLog(@"%@",newTextfield.text);
    }
}

- (void) hideKeyboard {
    NSArray *cells = self.tableView.visibleCells;
    for (UITableViewCell *cell in cells) {
        UITextField *textfield = (UITextField*)[cell.contentView viewWithTag:1];
        if([textfield isFirstResponder])
            [textfield resignFirstResponder];
    }
}
@end
