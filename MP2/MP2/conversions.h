//
//  conversions.h
//  MP2
//
//  Created by Zeyu Li on 3/28/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface conversions : NSObject

- (void) setDefaultValue: (NSString *)chooseType;
-(void) changeType: (NSString *)chooseType chooseUnit:(NSString *)chooseUnit value:(NSString *)value;

@property (nonatomic,strong)NSDictionary *dict;
@property (nonatomic,strong)NSDictionary *category;
@property (nonatomic,strong)NSMutableDictionary *resultdict;

@property (nonatomic,strong)NSArray *celsius;
@property (nonatomic,strong)NSArray *fahrenheit;
@property (nonatomic,strong)NSString *basictype;



@end
