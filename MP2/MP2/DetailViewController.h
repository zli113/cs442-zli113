//
//  DetailViewController.h
//  MP2
//
//  Created by Zeyu Li on 3/28/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (strong) NSDictionary *dict;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)doneButtonPressed:(id)sender;
- (IBAction)dynamicConversion:(id)sender;






@end
