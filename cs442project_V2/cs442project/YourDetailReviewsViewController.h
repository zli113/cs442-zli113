//
//  YourDetailReviewsViewController.h
//  cs442project
//
//  Created by Zeyu Li on 4/1/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface YourDetailReviewsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (strong,nonatomic)NSDate *date;
@property (nonatomic, strong) NSArray *menuItems;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic)NSManagedObjectContext *context;

@end
