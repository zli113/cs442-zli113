//
//  HomeViewController.h
//  cs442project
//
//  Created by Zeyu Li on 4/5/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface HomeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *menuItems;
- (IBAction)nextKeyboadrd:(id)sender;
- (IBAction)loginButtonPressed:(id)sender;


@end
