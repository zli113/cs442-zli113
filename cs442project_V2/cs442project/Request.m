//
//  Request.m
//  cs442project
//
//  Created by Zeyu Li on 4/25/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import "Request.h"


@implementation Request

@dynamic cwid;
@dynamic from;
@dynamic to;
@dynamic number;
@dynamic time;
@dynamic car;
@dynamic driver;
@dynamic state;
@dynamic message;

@end
