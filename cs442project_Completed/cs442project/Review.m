//
//  Review.m
//  cs442project
//
//  Created by Zeyu Li on 4/27/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import "Review.h"


@implementation Review

@dynamic cwid;
@dynamic category;
@dynamic part;
@dynamic time;
@dynamic review;

@end
