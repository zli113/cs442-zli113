//
//  ViewController.m
//  Connect4
//
//  Created by Michael Lee on 4/18/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import "ViewController.h"
#import "GameModel.h"

@interface ViewController ()
- (void)reinitGame;
@end

@implementation ViewController {
    GameModel *gameModel;
    NSMutableArray *pieces;
}

- (void)awakeFromNib
{
    pieces = [NSMutableArray array];
    gameModel = [[GameModel alloc] init];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.boardView = [[BoardView alloc] initWithFrame:self.view.bounds slotDiameter:40];
    self.boardView.delegate = self;
    [self.view addSubview:self.boardView];

        PFQuery *query = [PFQuery queryWithClassName:@"game"];
        [query getObjectInBackgroundWithId:self.gameId block:^(PFObject *game, NSError *error) {
            NSMutableArray *result=[[NSMutableArray alloc]init];
            result=game[@"data"];
            for(int row=0;row<6;row++)
            {
                for(int column=0;column<7;column++)
                {
                    if([[result objectAtIndex:row*7+column]integerValue]==2)
                    {
                        UIView *piece = [[UIView alloc]
                                         initWithFrame:CGRectMake(column*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                                                  -self.boardView.slotDiameter,
                                                                  self.boardView.slotDiameter,
                                                                  self.boardView.slotDiameter)];
                        [pieces addObject:piece];
                        piece.center = CGPointMake(column*self.boardView.gridWidth,
                                                   (6 - row)*self.boardView.gridHeight);
                        piece.backgroundColor = [UIColor redColor];
                        [self.view insertSubview:piece belowSubview:self.boardView];
                    }
                    if([[result objectAtIndex:row*7+column]integerValue]==1)
                    {
                        UIView *piece = [[UIView alloc]
                                         initWithFrame:CGRectMake(column*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                                                  -self.boardView.slotDiameter,
                                                                  self.boardView.slotDiameter,
                                                                  self.boardView.slotDiameter)];
                        [pieces addObject:piece];
                        piece.center = CGPointMake(column*self.boardView.gridWidth,
                                                   (6 - row)*self.boardView.gridHeight);
                        piece.backgroundColor = [UIColor yellowColor];
                        [self.view insertSubview:piece belowSubview:self.boardView];
                    }
                }
            }
        }];

}

- (void)reinitGame
{
    [gameModel resetGame];
    for (UIView *view in pieces) {
        [view removeFromSuperview];
    }
    [pieces removeAllObjects];
}

- (void)boardView:(BoardView *)boardView columnSelected:(int)column
{
    if(self.section==0||self.section==2)
    {
        if ([gameModel processTurnAtCol:column turnColor:self.color gameId:self.gameId]) {
            PFQuery *query2 = [PFQuery queryWithClassName:@"game"];
            [query2 getObjectInBackgroundWithId:self.gameId block:^(PFObject *game, NSError *error) {
                if (game[@"winner"]!=NULL) {
                        
                } else {
            
                int row = [[game[@"pieceInCol"]objectAtIndex:column-1]intValue];
                UIView *piece = [[UIView alloc]
                                 initWithFrame:CGRectMake(column*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                                          -self.boardView.slotDiameter,
                                                          self.boardView.slotDiameter,
                                                          self.boardView.slotDiameter)];
                [pieces addObject:piece];
                piece.backgroundColor = [gameModel colorForCurrentTurn];
                [self.view insertSubview:piece belowSubview:self.boardView];
                [UIView animateWithDuration:0.5 animations:^{
                    piece.center = CGPointMake(column*self.boardView.gridWidth,
                                               (6 - row)*self.boardView.gridHeight);
                }];
            
                }
            }];
        }
        PFQuery *query = [PFQuery queryWithClassName:@"game"];
        [query getObjectInBackgroundWithId:self.gameId block:^(PFObject *game, NSError *error) {
            if([self.color isEqualToString:@"red"])
                game[@"whoesTurn"] = game[@"player2"];
            if([self.color isEqualToString:@"yellow"])
                game[@"whoesTurn"] = game[@"player1"];
            [game saveInBackground];
        }];
        self.section=1;
    }
}
@end
