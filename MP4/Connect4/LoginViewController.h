//
//  LoginViewController.h
//  Connect4
//
//  Created by Zeyu Li on 5/8/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface LoginViewController : UIViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, UITableViewDataSource, UITableViewDelegate>
- (IBAction)logoutButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)newgameButtonPressed:(id)sender;


@end
