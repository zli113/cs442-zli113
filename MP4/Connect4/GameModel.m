//
//  GameInfo.m
//  Connect4
//
//  Created by Michael Lee on 5/1/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import "GameModel.h"

@interface GameModel ()
- (BOOL)checkForWinAtRow:(int)row col:(int)col state:(NSMutableArray *)test;
- (BOOL)checkForWinAtCol:(int)row col:(int)col state:(NSMutableArray *)test;
@end

@implementation GameModel {
    int _winner;
    int _turn;
    int _piecesInCol[7];
    int _board[6][7];
    NSMutableArray *array2;
}

- (id)init
{
    
    if (self = [super init]) {
        _turn = 1;
        _winner = 0;
        for (int i=0; i<7; i++) {
            _piecesInCol[i] = 0;
        }
        
    }
    return self;
}

- (void)resetGame
{
    _turn = 1;
    for (int i=0; i<7; i++) {
        _piecesInCol[i] = 0;
    }
}

- (int)turn
{
    return _turn;
}

- (UIColor *)colorForCurrentTurn
{
    if (_turn == 1) {
        return [UIColor yellowColor];
    } else {
        return [UIColor redColor];
    }
}

- (BOOL)checkForWinAtRow:(int)row col:(int)col state:(NSMutableArray *)test
{
    for(int i=0;i<test.count;i++)
        _board[i/7][i%7]=[[test objectAtIndex:i]intValue];
    int count=0;
    if(row>3){
    for(int i=row-1;i>=row-3;i--)
    {
        if(_board[i][col]==_turn)
            count++;
    }}
    return (count==3);
}
- (BOOL)checkForWinAtCol:(int)row col:(int)col state:(NSMutableArray *)test
{
    row=row-1;
    for(int i=0;i<test.count;i++)
        _board[i/7][i%7]=[[test objectAtIndex:i]intValue];
    int count=0;
    if(col>=4)
    {
        for(int i=col-1;i>=col-3;i--)
        {
            if(_board[row][i]==_turn)
                count++;
        }
    }
    return (count==3);
}

- (BOOL)isGameOver
{
    return (_winner != 0);
}

- (int)winner
{
    return _winner;
}

- (int *) getBoard
{
    return *_board;
}
- (BOOL)processTurnAtCol:(int)col turnColor:(NSString *)color gameId:(NSString *)gameId
{
    if ([self isGameOver]) {
        return NO;
    }
    if([color isEqualToString:@"red"])
        _turn=2;
    if([color isEqualToString:@"yellow"])
        _turn=1;
    if ([self topRowInCol:col] <= 5) {
        
        PFQuery *query = [PFQuery queryWithClassName:@"game"];
        [query getObjectInBackgroundWithId:gameId block:^(PFObject *game, NSError *error) {
            
            
            NSMutableArray *original=[[NSMutableArray alloc]init];
            original=game[@"data"];
            NSMutableArray *number=[[NSMutableArray alloc]init];
            number=game[@"pieceInCol"];
            if(number.count==0)
                _board[_piecesInCol[col]][col] = _turn;
            else
            {
                int row = [[number objectAtIndex:col-1]intValue];
                _board[row][col] = _turn;
            }
            
            if(original.count!=0)
            {
                //NSLog(@"%@",original);
                int originalBoard[6][7];
                for(int i=0;i<original.count;i++)
                    originalBoard[i/7][i%7]=[[original objectAtIndex:i]intValue];
                for(int i=0;i<6;i++)
                    for(int j=0;j<7;j++)
                        _board[i][j]=_board[i][j]+originalBoard[i][j];

            }
            NSMutableArray *array=[[NSMutableArray alloc]init];
            for(int i=0;i<6;i++)
                for(int j=0;j<7;j++)
                    [array addObject:[NSNumber numberWithInt:_board[i][j]]];
            game[@"data"]=array;
            for(int i=0;i<6;i++)
            {
                if(_board[i][col]!=0)
                    _piecesInCol[col]++;
            }
            
            array2=[[NSMutableArray alloc]initWithCapacity:7];
            if(number.count==0){
                for (int i = 0; i < 7; i++)
                    [array2 addObject:[NSNumber numberWithInt:0]];
                }
            if(number.count!=0)
                for (int i = 0; i < 7; i++)
                    [array2 insertObject:[number objectAtIndex:i]  atIndex:i];
            //array2=number;
            [array2 replaceObjectAtIndex:col-1 withObject:[NSNumber numberWithInt:_piecesInCol[col]]];
            game[@"pieceInCol"]=array2;
            if ([self checkForWinAtRow:[[array2 objectAtIndex:col-1]intValue] col:col state:array]||[self checkForWinAtCol:[[array2 objectAtIndex:col-1]intValue] col:col state:array]) {
                                if(_turn==2)
                    game[@"winner"]=game[@"player1"];
                else
                    game[@"winner"]=game[@"player2"];
                game[@"whoesTurn"]=@"Game Over";
                NSString *message = [game[@"winner"] stringByAppendingString:@" Wins"];
                UIAlertView*alert = [[UIAlertView alloc]initWithTitle:@"Game Over"
                                                              message:message
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
                [alert show];
                
            }
            [game saveInBackground];
        }];
        //_turn = (_turn == 1)? 2 : 1;
        return YES;
    } else {
        return NO;
    }
}

- (int)topRowInCol:(int)col
{
    return _piecesInCol[col];
}
@end
