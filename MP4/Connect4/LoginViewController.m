//
//  LoginViewController.m
//  Connect4
//
//  Created by Zeyu Li on 5/8/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import "LoginViewController.h"
#import "ViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController
{
    UILabel *player1;
    UILabel *player2;
    NSMutableArray *yourTurn;
    NSMutableArray *theirTurn;
    NSMutableArray *availableGames;
    NSMutableArray *finishedGames;
    PFUser *currentUser;
}

- (void) awakeFromNib
{
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.opaque = NO;
    yourTurn =[[NSMutableArray alloc]init];
    theirTurn =[[NSMutableArray alloc]init];
    availableGames =[[NSMutableArray alloc]init];
    finishedGames =[[NSMutableArray alloc]init];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (![PFUser currentUser]) {
        PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
        [logInViewController setDelegate:self];
        PFSignUpViewController *signUpViewController = [[PFSignUpViewController alloc] init];
        [signUpViewController setDelegate:self]; 
        [logInViewController setSignUpController:signUpViewController];
        [self presentViewController:logInViewController animated:YES completion:NULL];
    }
    if([yourTurn count]!=0)
        [yourTurn removeAllObjects];
    if([theirTurn count]!=0)
        [theirTurn removeAllObjects];
    if([availableGames count]!=0)
        [availableGames removeAllObjects];
    if([finishedGames count]!=0)
        [finishedGames removeAllObjects];
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:
                               @"whoesTurn = %@",currentUser.username];
    PFQuery *query1 = [PFQuery queryWithClassName:@"game" predicate:predicate1];
    [query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if ([objects count]!=0) {
            for (PFObject *object in objects) {
                [yourTurn addObject:object];
                [self.tableView reloadData];
            }
        }
    }];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:
                                   @"whoesTurn != %@ AND (player1 = %@ OR player2 = %@) AND winner==NULL",  currentUser.username, currentUser.username, currentUser.username];
    PFQuery *query2 = [PFQuery queryWithClassName:@"game" predicate:predicate2];
    [query2 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if ([objects count]!=0) {
            for (PFObject *object in objects) {
                [theirTurn addObject:object];
                [self.tableView reloadData];
            }
        }
    }];
    NSPredicate *predicate3 = [NSPredicate predicateWithFormat:
                               @"player1 != %@ AND player2 != %@ AND (player1 = 'No opponent yet' OR player2 = 'No opponent yet')", currentUser.username, currentUser.username];
    PFQuery *query3 = [PFQuery queryWithClassName:@"game" predicate:predicate3];
    [query3 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if ([objects count]!=0) {
            for (PFObject *object in objects) {
                [availableGames addObject:object];
                [self.tableView reloadData];
            }
        }
    }];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"winner!=NULL"];
    PFQuery *query4 = [PFQuery queryWithClassName:@"game" predicate:predicate];
    [query4 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if ([objects count]!=0) {
            for (PFObject *object in objects) {
                [finishedGames addObject:object];
                [self.tableView reloadData];
            }
        }
    }];
    
}

- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    // Check if both fields are completed
    if (username && password && username.length != 0 && password.length != 0) {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:@"Missing Information"
                                message:@"Make sure you fill out all of the information!"
                               delegate:nil
                      cancelButtonTitle:@"ok"
                      otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL];
    currentUser = [PFUser currentUser];
    if([yourTurn count]!=0)
        [yourTurn removeAllObjects];
    if([theirTurn count]!=0)
        [theirTurn removeAllObjects];
    if([availableGames count]!=0)
        [availableGames removeAllObjects];
    if([finishedGames count]!=0)
        [finishedGames removeAllObjects];
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:
                                @"whoesTurn = %@",  currentUser.username];
    PFQuery *query1 = [PFQuery queryWithClassName:@"game" predicate:predicate1];
    [query1 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                
                [yourTurn addObject:object];
               [self.tableView reloadData];
            }
        } else {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:
                               @"whoesTurn != %@ AND (player1 = %@ OR player2 = %@) AND winner==NULL",  currentUser.username, currentUser.username, currentUser.username];
    PFQuery *query2 = [PFQuery queryWithClassName:@"game" predicate:predicate2];
    //[query2 whereKey:@"whoesTurn" notEqualTo:currentUser.username];
    [query2 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                [theirTurn addObject:object];
                [self.tableView reloadData];
            }
        } else {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    NSPredicate *predicate3 = [NSPredicate predicateWithFormat:
                               @"player1 != %@ AND player2 != %@ AND (player1 = 'No opponent yet' OR player2 = 'No opponent yet')", currentUser.username, currentUser.username];
    PFQuery *query3 = [PFQuery queryWithClassName:@"game" predicate:predicate3];
    [query3 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                
                [availableGames addObject:object];
                [self.tableView reloadData];
            }
        } else {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              @"winner!=NULL"];
    PFQuery *query4 = [PFQuery queryWithClassName:@"game" predicate:predicate];
    [query4 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            for (PFObject *object in objects) {
                
                [finishedGames addObject:object];
                [self.tableView reloadData];
            }
        } else {
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
}

- (IBAction)logoutButtonPressed:(id)sender {
    [PFUser logOut];
    [self viewDidAppear:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
        return [yourTurn count];
    if(section==1)
        return [theirTurn count];
    if(section==2)
        return [availableGames count];
    else
        return [finishedGames count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section==0)
        return @"Your Turn";
    if(section==1)
        return @"Their Turn";
    if(section==2)
        return @"Availble Games";
    else
        return @"Finished Games";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(indexPath.section==0 && [yourTurn count]!=0)
    {
        if([cell.contentView viewWithTag:1])
            player1 = (UILabel*)[cell.contentView viewWithTag:1];
        player1.text=[[yourTurn objectAtIndex:indexPath.row]objectForKey:@"player1"];
        if([cell.contentView viewWithTag:2])
            player2 = (UILabel*)[cell.contentView viewWithTag:2];
        player2.text=[[yourTurn objectAtIndex:indexPath.row]objectForKey:@"player2"];
    }
    if(indexPath.section==1 && [theirTurn count]!=0)
    {
        if([cell.contentView viewWithTag:1])
            player1 = (UILabel*)[cell.contentView viewWithTag:1];
        player1.text=[[theirTurn objectAtIndex:indexPath.row]objectForKey:@"player1"];
        if([cell.contentView viewWithTag:2])
            player2 = (UILabel*)[cell.contentView viewWithTag:2];
        player2.text=[[theirTurn objectAtIndex:indexPath.row]objectForKey:@"player2"];
    }
    if(indexPath.section==3 && [finishedGames count]!=0)
    {
        if([cell.contentView viewWithTag:1])
            player1 = (UILabel*)[cell.contentView viewWithTag:1];
        player1.text=[[finishedGames objectAtIndex:indexPath.row]objectForKey:@"player1"];
        if([cell.contentView viewWithTag:2])
            player2 = (UILabel*)[cell.contentView viewWithTag:2];
        player2.text=[[finishedGames objectAtIndex:indexPath.row]objectForKey:@"player2"];
    }
    return cell;
}


- (CGFloat )tableView:( UITableView *)tableView heightForHeaderInSection:( NSInteger )section
{
    return 44;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ViewController *dest=segue.destinationViewController;
    NSInteger selectedSection=[self.tableView indexPathForSelectedRow].section;
    NSInteger selectedRow=[self.tableView indexPathForSelectedRow].row;
    dest.section=selectedSection;
    if([player1.text isEqualToString:currentUser.username])
        dest.color=@"red";
    if([player2.text isEqualToString:currentUser.username])
        dest.color=@"yellow";
    if(selectedSection==0)
        dest.gameId = [[yourTurn objectAtIndex:selectedRow]valueForKeyPath:@"objectId"];
    if(selectedSection==1)
        dest.gameId = [[theirTurn objectAtIndex:selectedRow]valueForKeyPath:@"objectId"];
    if(selectedSection==2)
    {
        dest.color=@"yellow";
        dest.gameId = [[availableGames objectAtIndex:selectedRow]valueForKeyPath:@"objectId"];
        PFQuery *query = [PFQuery queryWithClassName:@"game"];
        [query getObjectInBackgroundWithId:dest.gameId block:^(PFObject *game, NSError *error) {
            if([player1.text isEqualToString:@"No opponent yet"])
                game[@"player1"]=currentUser.username;
            if([player2.text isEqualToString:@"No opponent yet"])
                game[@"player2"]=currentUser.username;
            if([game[@"whoesTurn"]isEqualToString:@"No opponent yet"])
                game[@"whoesTurn"]=currentUser.username;
            [game saveInBackground];
        }];
            }
    if(selectedSection==3)
    {
        dest.gameId = [[finishedGames objectAtIndex:selectedRow]valueForKeyPath:@"objectId"];
        PFQuery *query = [PFQuery queryWithClassName:@"game"];
        [query getObjectInBackgroundWithId:dest.gameId block:^(PFObject *game, NSError *error) {
            NSString *message=[@"Winner is " stringByAppendingString:game[@"winner"]];
            UIAlertView*alert = [[UIAlertView alloc]initWithTitle:@"The game has been finished"
                                                          message:message
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
            [alert show];
        }];
    }
}
- (IBAction)newgameButtonPressed:(id)sender {
    PFObject *newGame = [PFObject objectWithClassName:@"game"];
    newGame[@"player1"]=currentUser.username;
    newGame[@"player2"]=@"No opponent yet";
    newGame[@"whoesTurn"]=currentUser.username;
    [newGame saveInBackground];
    [yourTurn addObject:newGame];
    [self.tableView reloadData];
}
@end
