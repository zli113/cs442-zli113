//
//  ViewController.h
//  Connect4
//
//  Created by Michael Lee on 4/18/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BoardView.h"

@interface ViewController : UIViewController <BoardViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) BoardView *boardView;
@end
