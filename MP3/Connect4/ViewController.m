//
//  ViewController.m
//  Connect4
//
//  Created by Michael Lee on 4/18/14.
//  Copyright (c) 2014 Michael Lee. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
{
    NSMutableArray * countColumn;
    NSMutableArray * countPiece;
    NSString *tag;
}

- (void)viewDidAppear:(BOOL)animated
{
    countColumn=[[NSMutableArray alloc]initWithObjects:@0,@0,@0,@0,@0,@0,@0,@0,nil];
    countPiece=[[NSMutableArray alloc]initWithCapacity:45];
    for(int i=0;i<45;i++)
        [countPiece insertObject:@"null" atIndex:i];
    [super viewDidAppear:animated];
    self.boardView = [[BoardView alloc] initWithFrame:self.view.bounds slotDiameter:40];
    self.boardView.delegate = self;
    [self.view addSubview:self.boardView];
}

- (void)boardView:(BoardView *)boardView columnSelected:(int)column
{
    UIView *piece = [[UIView alloc]
                     initWithFrame:CGRectMake(column*self.boardView.gridWidth-self.boardView.slotDiameter/2.0,
                                              -self.boardView.slotDiameter,
                                              self.boardView.slotDiameter,
                                              self.boardView.slotDiameter)];
    int x = [[countColumn objectAtIndex:column]intValue];
    
    if(self.boardView.count%2==1)
    {
        piece.backgroundColor = [UIColor yellowColor];
        [countPiece replaceObjectAtIndex:x*7+column withObject:@"yellow"];
    }
    else
    {
        piece.backgroundColor = [UIColor redColor];
        [countPiece replaceObjectAtIndex:x*7+column withObject:@"red"];
    }
    [self.view insertSubview:piece belowSubview:self.boardView];
    [UIView animateWithDuration:2.0 animations:^{
        piece.center = CGPointMake(column*self.boardView.gridWidth, (6-x)*self.boardView.gridHeight);
    }];
    for(int i=0;i<6;i++)
    {
        
        for(int j=1;j<=4;j++)
        {
            if(([[countPiece objectAtIndex:i*7+j]isEqualToString:@"yellow"]&&[[countPiece objectAtIndex:i*7+j+1]isEqualToString:@"yellow"]&&[[countPiece objectAtIndex:i*7+j+2]isEqualToString:@"yellow"]&&[[countPiece objectAtIndex:i*7+j+3]isEqualToString:@"yellow"])
               ||
               (i<=2&&[[countPiece objectAtIndex:i*7+j]isEqualToString:@"yellow"]&&[[countPiece objectAtIndex:(i+1)*7+j]isEqualToString:@"yellow"]&&[countPiece objectAtIndex:(i+2)*7+j]&&[[countPiece objectAtIndex:(i+3)*7+j]isEqualToString:@"yellow"])
               ||
               (i<=2&&[[countPiece objectAtIndex:i*7+j]isEqualToString:@"yellow"]&&[[countPiece objectAtIndex:(i+1)*7+j+1]isEqualToString:@"yellow"]&&[[countPiece objectAtIndex:(i+2)*7+j+2]isEqualToString:@"yellow"]&&[[countPiece objectAtIndex:(i+3)*7+j+3]isEqualToString:@"yellow"])
               ||(i<=2&&j==4&&[[countPiece objectAtIndex:i*7+j]isEqualToString:@"yellow"]&&[[countPiece objectAtIndex:(i+1)*7+j-1]isEqualToString:@"yellow"]&&[[countPiece objectAtIndex:(i+2)*7+j-2]isEqualToString:@"yellow"]&&[[countPiece objectAtIndex:(i+3)*7+j-3]isEqualToString:@"yellow"]))
                {
                    double delayInSeconds = 2.0;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        tag=@"Game Over";
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:tag message:@"Yellow Wins" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Play Again", nil];
                        [alert show];
                    });
                    
                }
            if(([[countPiece objectAtIndex:i*7+j]isEqualToString:@"red"]&&[[countPiece objectAtIndex:i*7+j+1]isEqualToString:@"red"]&&[[countPiece objectAtIndex:i*7+j+2]isEqualToString:@"red"]&&[[countPiece objectAtIndex:i*7+j+3]isEqualToString:@"red"])
               ||
               (i<=2&&[[countPiece objectAtIndex:i*7+j]isEqualToString:@"red"]&&[[countPiece objectAtIndex:(i+1)*7+j]isEqualToString:@"red"]&&[countPiece objectAtIndex:(i+2)*7+j]&&[[countPiece objectAtIndex:(i+3)*7+j]isEqualToString:@"red"])
               ||
               (i<=2&&[[countPiece objectAtIndex:i*7+j]isEqualToString:@"red"]&&[[countPiece objectAtIndex:(i+1)*7+j+1]isEqualToString:@"red"]&&[[countPiece objectAtIndex:(i+2)*7+j+2]isEqualToString:@"red"]&&[[countPiece objectAtIndex:(i+3)*7+j+3]isEqualToString:@"red"])
               ||
               (i<=2&&j==4&&[[countPiece objectAtIndex:i*7+j]isEqualToString:@"red"]&&[[countPiece objectAtIndex:(i+1)*7+j-1]isEqualToString:@"red"]&&[[countPiece objectAtIndex:(i+2)*7+j-2]isEqualToString:@"red"]&&[[countPiece objectAtIndex:(i+3)*7+j-3]isEqualToString:@"red"]))
                {
                    double delayInSeconds = 1.5;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        tag=@"Game Over";
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:tag message:@"Red Wins" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Play Again", nil];
                        [alert setTag:0];
                        [alert show];
                    });
                }
        }
    }
    if(![tag isEqualToString:@"Gave Over"])
    {
        if(x<=5){
            x = x + 1;
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Out of bounds" delegate:self cancelButtonTitle:@"Try Again" otherButtonTitles:nil];
            [alert setTag:1];
            [alert show];
        }
        [countColumn replaceObjectAtIndex:column withObject:[NSNumber numberWithInt:x]];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==0)
    {
        if(buttonIndex ==1)
        {
            for(UIView *subview in [self.view subviews]) {
                if([subview isKindOfClass:[UIView class]]) {
                    [subview removeFromSuperview];
                }
            }
            [self viewDidAppear:YES];
        }
    }
}

@end
