//
//  MainViewController.m
//  cs442project
//
//  Created by Zeyu Li on 3/31/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import "MainViewController.h"
#import "SWRevealViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

@synthesize sidebarButton=_sidebarButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.title = @"IIT Services";
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    self.view.backgroundColor = [UIColor whiteColor];
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    // MapView
    self.myMapView.showsUserLocation = YES;
//    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
//    locationManager.delegate=self;
//    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
//    locationManager.distanceFilter=1000.0f;
//    [locationManager startUpdatingLocation];
//    MKCoordinateSpan theSpan;
//    theSpan.latitudeDelta=0.05;
//    theSpan.longitudeDelta=0.05;
//    MKCoordinateRegion theRegion;
//    theRegion.center=[[locationManager location] coordinate];
//    theRegion.span=theSpan;
//    [self.myMapView setRegion:theRegion];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
