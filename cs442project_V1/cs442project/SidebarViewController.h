//
//  SidebarViewController.h
//  cs442project
//
//  Created by Zeyu Li on 3/31/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface SidebarViewController : UITableViewController

@property (nonatomic, strong) NSArray *menuItems;
@property (strong)NSString *username;
@end
