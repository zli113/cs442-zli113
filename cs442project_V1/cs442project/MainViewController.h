//
//  MainViewController.h
//  cs442project
//
//  Created by Zeyu Li on 3/31/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MainViewController : UIViewController<CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem * sidebarButton;
@property (strong, nonatomic) IBOutlet MKMapView *myMapView;

@end
