//
//  ViewController.m
//  MP1_Zeyu_Li
//
//  Created by Zeyu Li on 3/1/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.convertButton setTitle:NSLocalizedString(@"CONVERT",nil) forState:UIControlStateNormal];
    [self.categorySegment setTitle:NSLocalizedString(@"TEMPERATURE",nil) forSegmentAtIndex:0];
    [self.categorySegment setTitle:NSLocalizedString(@"DISTANCE",nil) forSegmentAtIndex:1];
    [self.categorySegment setTitle:NSLocalizedString(@"VOLUME",nil) forSegmentAtIndex:2];
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
static convert _con[][2]={
    {
        ^(double f){
            return (f-32.0)*5.0/9.0;
        },
        ^(double c){
            return c*9.0/5.0+32.0;
        },
    },
    {
        ^(double miles){
            return miles*1.609344;
        },
        ^(double kilometers){
            return kilometers/1.609344;
        },
    },
    {
        ^(double gallons){
            return gallons*3.78541;
        },
        ^(double liters){
            return liters/3.78541;
        }
    }
};

- (IBAction)convertButtonPressed:(id)sender {
    if([self.topField isFirstResponder])
    {
        double value=[self.topField.text doubleValue];
        value=_con[self.categorySegment.selectedSegmentIndex][0](value);
        self.bottomField.text=[NSString stringWithFormat:@"%.2f",value];
        [self.topField resignFirstResponder];
    }
    if([self.bottomField isFirstResponder])
    {
        double value=[self.bottomField.text doubleValue];
        value=_con[self.categorySegment.selectedSegmentIndex][1](value);
        self.topField.text=[NSString stringWithFormat:@"%.2f",value];
        [self.bottomField resignFirstResponder];
    }
}

- (IBAction)segmentChanged:(id)sender {
    int index=(int)self.categorySegment.selectedSegmentIndex;
    if([self.convertDirectionButton.currentTitle isEqual:@"⬇︎"]&&self.topField.text.length!=0)
        self.bottomField.text=[NSString stringWithFormat:@"%.2f",_con[index][0]([self.topField.text doubleValue])];
    else if([self.convertDirectionButton.currentTitle isEqual:@"⬆︎"]&&self.bottomField.text.length!=0)
        self.topField.text=[NSString stringWithFormat:@"%.2f",_con[index][1]([self.bottomField.text doubleValue])];
    if(index==0)
    {
        self.topLable.text=@"℉";
        self.bottomLable.text=@"℃";
    }
    if(index==1)
    {
        self.topLable.text=NSLocalizedString(@"MILES",nil);
        self.bottomLable.text=NSLocalizedString(@"KILOMETERS",nil);
    }
    if(index==2)
    {
        self.topLable.text=NSLocalizedString(@"GALLONS",nil);
        self.bottomLable.text=NSLocalizedString(@"LITERS",nil);
    }
}

- (IBAction)onBackgroundHit:(id)sender {
    NSArray *subviews=[self.view subviews];
    for (id objInput in subviews) {
        if ([objInput isKindOfClass:[UITextField class]]) {
            UITextField *theTextField=objInput;
            if ([objInput isFirstResponder]) {
                [theTextField resignFirstResponder];
            }
        }
    }
}

- (IBAction)dynamicChanged:(id)sender {
    if([self.topField isFirstResponder])
    {
        double value=[self.topField.text doubleValue];
        value=_con[self.categorySegment.selectedSegmentIndex][0](value);
        self.bottomField.text=[NSString stringWithFormat:@"%.2f",value];
    }
    if([self.bottomField isFirstResponder])
    {
        double value=[self.bottomField.text doubleValue];
        value=_con[self.categorySegment.selectedSegmentIndex][1](value);
        self.topField.text=[NSString stringWithFormat:@"%.2f",value];
    }
}

-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField==self.topField)
        [self.convertDirectionButton setTitle:@"⬇︎" forState:UIControlStateNormal];
    else
        [self.convertDirectionButton setTitle:@"⬆︎" forState:UIControlStateNormal];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}
@end
