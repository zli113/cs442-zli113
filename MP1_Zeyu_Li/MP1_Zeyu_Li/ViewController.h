//
//  ViewController.h
//  MP1_Zeyu_Li
//
//  Created by Zeyu Li on 3/1/14.
//  Copyright (c) 2014 Zeyu Li. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UISegmentedControl *categorySegment;
@property (weak, nonatomic) IBOutlet UITextField *topField;
@property (weak, nonatomic) IBOutlet UILabel *topLable;
@property (weak, nonatomic) IBOutlet UITextField *bottomField;
@property (weak, nonatomic) IBOutlet UILabel *bottomLable;
@property (weak, nonatomic) IBOutlet UIButton *convertButton;
@property (weak, nonatomic) IBOutlet UIButton *convertDirectionButton;
- (IBAction)convertButtonPressed:(id)sender;
- (IBAction)segmentChanged:(id)sender;
- (IBAction)onBackgroundHit:(id)sender;
- (IBAction)dynamicChanged:(id)sender;
typedef double (^convert)(double);
@end
